import HttpException from "../exceptions/Http.exception.js";
import log from "@ajar/marker";
import fs from "fs";
import { NextFunction, Response, Request } from "express";
import UrlNotFoundException from "../exceptions/UrlNotFound.exception.js";
import { failureResponse } from "../modules/common/service.js";

// const { NODE_ENV } = process.env;

export function printErrorMiddleware(
    error: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) {
    log.red(error.status, error.stack);
    // log.red(error.status, error.message);
    // log.red(error.stack);

    next(error);
}

export function logErrorMiddleware(path: string) {
    // https://nodejs.org/api/fs.html#file-system-flags
    // 'a': Open file for appending. The file is created if it does not exist.
    const errorsFileLogger = fs.createWriteStream(path, { flags: "a" });

    return (
        error: HttpException,
        req: Request,
        res: Response,  
        next: NextFunction
    ) => {
        // new log entry ->

        errorsFileLogger.write(
            `${error.status} :: ${error.message} >> ${error.stack} \n`
        );
        // failureResponse(error,res);
        next(error);
    };
}

export function respondWithErrorMiddleware(
    error: HttpException,
    req: Request,
    res: Response,
) {
    console.log("AAASSS");
    failureResponse(error,res);
}

export function urlNotFoundMiddleware(
    req: Request,
    res: Response,
    next: NextFunction
) {
    next(new UrlNotFoundException(req.url));
}
