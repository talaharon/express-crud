import { NextFunction, Request, Response } from "express";
import fsp from "fs/promises";

class CommonMiddlewares {
    logger =
        (path: string) =>
        async (req: Request, res: Response, next: NextFunction) => {
            await fsp.appendFile(
                path,
                "[" + +Date.now() + "] " + req.method + " " + req.path + "\n"
            );
            next();
        };
}

export default new CommonMiddlewares();
