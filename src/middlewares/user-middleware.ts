import { NextFunction, Request, Response } from "express";
import DBException from "../exceptions/DB.exception.js";
import UserNotExistsException from "../exceptions/UserNotExist.exception.js";
import userService from "../modules/users/user-service.js";

class UserMiddleware {
    private userService = userService.getInstance();

    getAllUsers = async (req: Request, res: Response, next: NextFunction) => {
        if (await this.userService.updateUsersList()) {
            next();
        } else {
            next(new DBException("read"));
        }
    };

    userExists = async (req: Request, res: Response, next: NextFunction) => {
        if (await this.userService.userExists(req.params.id)) {
            next();
        } else {
            next(new UserNotExistsException(req.params.id));
        }
    };

}

export default new UserMiddleware();
