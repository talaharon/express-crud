import { Request, Response } from "express";
import DBException from "../exceptions/DB.exception.js";
import { successResponse } from "../modules/common/service.js";
import UserModel from "../modules/users/user-service.js";

class UsersController {
    private userService = UserModel.getInstance();

    getUserById = async (req: Request, res: Response) => {
        const userToFind = await this.userService.getUserById(req.params.id);
        successResponse("User returned",userToFind,res);
    };

    deleteUserById = async (req: Request, res: Response) => {
        const userToDelete = this.userService.getUserById(req.params.id);
        if(await this.userService.deleteUserById(req.params.id)){
            successResponse("User deleted successfully",userToDelete,res);
        }else{
            throw new DBException("delete");
        }
    };

    showUsers = async (req: Request, res: Response) => {
        successResponse("Users sent successfully",this.userService.users,res);
    };

    updateUser = async (req: Request, res: Response) => {
        const ans = await this.userService.updateUser(
            req.params.id,
            req.params.name,
            req.params.lastname
        );
        const userUpdated = {
            id: req.params.id,
            name: req.params.name,
            lastname: req.params.lastname
        };
        if(ans){
            successResponse("User updated successfully",userUpdated,res);
        }else{
            throw new DBException("update");
        }
    };

    addUser = async (req: Request, res: Response) => {
        const ans = await this.userService.addUser(req.params.name, req.params.lastname);
        if(ans){
            successResponse("User added successfully",this.userService.users,res);
        }else{
            throw new DBException("add");
        }
    };
}

export default new UsersController();
