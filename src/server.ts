import app from "./config/app.js";

const { PORT = "3000", HOST = "localhost" } = process.env;

app.listen(Number(PORT),HOST, () => {
   console.log("Express server listening on port " + PORT);
});