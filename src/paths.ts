import path from "path";
import { fileURLToPath } from "url";

export const __filename = fileURLToPath(import.meta.url);
export const __dirname = path.dirname(__filename);
export const DB_PATH = path.resolve(__dirname, "../users.json");
export const LOG_PATH = path.resolve(__dirname, "../logs/users.http.log.txt");
export const ERRORS_LOG_FILE_PATH = path.resolve(__dirname, "../logs/users.errors.log");
