import fsp from "fs/promises";
import { DB_PATH } from "../../paths.js";
import { IUser } from "./user-model.js";
import { uidGenerator } from "../utils.js";

export default class UserModel {
    private static instance: UserModel;
    users: IUser[] = [];

    private constructor() {
        // Singleton
    }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): UserModel {
        if (!UserModel.instance) {
            UserModel.instance = new UserModel();
        }

        return UserModel.instance;
    }

    updateUsersList = async (): Promise<boolean> => {
        try {
            this.users = await this.load();
            return true;
        } catch (err) {
            return false;
        }
    };

    getUserById = async (id: string) => {
        return this.users.find((user) => id === user.id);
    };

    deleteUserById = async (id: string): Promise<boolean> => {
        try {
            await this.save(this.users.filter((user) => user.id !== id));
            return true;
        } catch (err) {
            return false;
        }
    };

    updateUser = async (
        id: string,
        name: string,
        lastname: string
    ): Promise<boolean> => {
        const userIndex = this.users.findIndex((user: IUser) => {
            return user.id === id;
        });
        this.users[userIndex].name = name;
        this.users[userIndex].lastname = lastname;
        try {
            await this.save(this.users);
            return true;
        } catch (err) {
            return false;
        }
    };

    addUser = async (name: string, lastname: string) => {
        const userToAdd = {
            id: uidGenerator(),
            name,
            lastname,
        };
        this.users.push(userToAdd);
        try {
            await this.save(this.users);
            return true;
        } catch (err) {
            return false;
        }
    };

    userExists = async (id: string): Promise<boolean> => {
        const index = this.users.findIndex((user) => id === user.id);
        if (index !== -1) {
            return true;
        } else {
            return false;
        }
    };

    /**
     * DB related functions
     */

    private init = async () => {
        try {
            await fsp.access(DB_PATH);
        } catch (error) {
            await fsp.writeFile(DB_PATH, JSON.stringify([]));
        }
    };

    private load = async () => {
        return JSON.parse(await fsp.readFile(DB_PATH, "utf-8"));
    };

    private save = async (data: unknown) => {
        await fsp.writeFile(DB_PATH, JSON.stringify(data));
    };
}
