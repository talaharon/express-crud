import { Response } from "express";
import HttpException from "../../exceptions/Http.exception.js";
import { response_status_codes } from "./model.js";

export function successResponse(message: string, DATA: unknown, res: Response) {
    res.status(response_status_codes.success).json({
        STATUS: "SUCCESS",
        MESSAGE: message,
        DATA
    });
}

export function failureResponse(err:HttpException,res:Response) {
    res.status(err.status).json({
        STATUS: "FAILURE",
        MESSAGE: err.message,
        DATA:[]
    });
}

// export function mongoError(err: any, res: Response) {
//     res.status(response_status_codes.internal_server_error).json({
//         STATUS: "FAILURE",
//         MESSAGE: "MongoDB error",
//         DATA: err
//     });
// }