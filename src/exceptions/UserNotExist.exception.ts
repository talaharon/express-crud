import HttpException from "./Http.exception.js";

class UserNotExistsException extends HttpException {
    constructor(id: string) {
        super(400, `User with ID ${id} not found`);
    }
}

export default UserNotExistsException;
