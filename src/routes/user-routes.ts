import express from "express";
import UsersController from "../controllers/users-controller.js";
import UserMiddleware from "../middlewares/user-middleware.js";

const usersRouter = express.Router();

/**
 * Read
 */
usersRouter.get("/", UserMiddleware.getAllUsers, UsersController.showUsers);

usersRouter.get(
    "/:id",
    UserMiddleware.getAllUsers,
    UserMiddleware.userExists,
    UsersController.getUserById
);

/**
 * Create
 */
usersRouter.post(
    "/:name/:lastname",
    UserMiddleware.getAllUsers,
    UsersController.addUser
);

/**
 * Update
 */
usersRouter.put(
    "/:id/:name/:lastname",
    UserMiddleware.getAllUsers,
    UserMiddleware.userExists,
    UsersController.updateUser
);

/**
 * Delete
 */
usersRouter.delete(
    "/:id",
    UserMiddleware.getAllUsers,
    UserMiddleware.userExists,
    UsersController.deleteUserById
);



export default usersRouter;
