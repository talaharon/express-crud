import express from "express";
import UrlNotFoundException from "../exceptions/UrlNotFound.exception.js";

const commonRouter = express.Router();

// Mismtached URL
commonRouter.all("*", (req,res,next) => {
    next(new UrlNotFoundException(req.baseUrl));
});

export default commonRouter;
