import express from "express";
import usersRouter from "../routes/user-routes.js";
import commonRouter from "../routes/common-routes.js";
import morgan from "morgan";
import commonMiddlewares from "../middlewares/common-middlewares.js";
import { DB_PATH, ERRORS_LOG_FILE_PATH, LOG_PATH } from "../paths.js";
import {
    logErrorMiddleware,
    printErrorMiddleware,
    respondWithErrorMiddleware,
    urlNotFoundMiddleware,
} from "../middlewares/error-middlewares.js";
import fsp from "fs/promises";

class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.configGeneral();
        this.configMiddlewares();
        this.configRoutes();
        this.configErrorHandlers();
        this.dbSetup();
    }

    configErrorHandlers() {
        this.app.use(urlNotFoundMiddleware);
        this.app.use(printErrorMiddleware);
        this.app.use(logErrorMiddleware(ERRORS_LOG_FILE_PATH));
        this.app.use(respondWithErrorMiddleware);
    }

    private configGeneral() {
        this.app.use(morgan("dev"));
        this.app.use(express.json());
    }

    private configRoutes() {
        this.app.use("/api/users", usersRouter);
        this.app.use("*", commonRouter);
    }

    private configMiddlewares() {
        this.app.use(commonMiddlewares.logger(LOG_PATH));
    }

    private async dbSetup() {
        console.log(DB_PATH);
        try {
            await fsp.access(DB_PATH);
        } catch (error) {
            await fsp.writeFile(DB_PATH, JSON.stringify([]));
        }
    }
}

export default new App().app;
